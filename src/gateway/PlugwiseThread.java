package gateway;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import plugwise.core.*;

public class PlugwiseThread implements Runnable {

	PlugwiseWrapper plugwise;
	SensorReading sensBuff;
	String serialPort;
	HashMap<String, String> plugwiseMap;
	double thr;
	String MACs;
	LinkedList<String> names;

	public PlugwiseThread(SensorReading sensBuff, String serialPort, HashMap<String, String> map, double thr)
	{
		this.sensBuff = sensBuff;
		this.serialPort = serialPort;
		this.plugwiseMap = map;
		this.thr = thr;
		this.MACs = "";

		Iterator<String> i = plugwiseMap.values().iterator();
		while(i.hasNext())
			MACs = MACs + i.next() + ",";
		MACs = MACs.replaceAll(",$", "");
		System.out.println("MACs: " + MACs);
		Thread t = new Thread(this);
		t.start();
	}
	
	@Override
	public void run() {
		plugwise = new PlugwiseWrapper(serialPort);
		while(true)
		{
			/*
				String name = i.next();
				String MAC = plugwiseMap.get(name);
				PlugStatus status = plugwise.livepowerPlug(MAC);
				System.out.println(status);

				if (status != null)
				{
					boolean state = false;
					System.out.println(status);
					if (status.currentPower > thr)
						state = true;
					sensBuff.push(name, state);
				}
			 */

			LinkedList<PlugStatus> list = plugwise.livepowerAllPlugs(MACs);
			Iterator<PlugStatus> iPlug = list.iterator();
			while(iPlug.hasNext())
			{
				PlugStatus status = iPlug.next();
				boolean state = false;
				System.out.println(status);
				if (status.currentPower >= thr)
					state = true;
				Iterator<String> iKey = plugwiseMap.keySet().iterator();
				while (iKey.hasNext()){
					String name = iKey.next();
					if (plugwiseMap.get(name).equalsIgnoreCase(status.mac))
					{
						sensBuff.push(name, state);
					}
				}
			}
		}
	}
}