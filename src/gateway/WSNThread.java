package gateway;
import wsn.*;

public class WSNThread implements Runnable {
	
	Oscilloscope wsnCore;
	SensorReading sensBuff;
	String serialPort;
	public WSNThread(SensorReading sensBuff, String serialPort)
	{
		this.sensBuff = sensBuff;
		this.serialPort = serialPort;
		Thread t = new Thread(this);
		t.start();
	}
	
	@Override
	public void run() {
		wsnCore = new Oscilloscope(sensBuff, serialPort);
	}

}
