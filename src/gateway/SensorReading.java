package gateway;

import com.google.gson.*;
import java.util.*;

import wsn.Oscilloscope;

public class SensorReading 
{
	public class Reading
	{
		public String sensID;
		public boolean value;

		public Reading(String sensID, boolean value)
		{
			this.sensID = sensID;
			this.value = value;
		}
	}
	

	private HashMap<String,Boolean> store;
	private HashMap<String, Integer> windowPIR;
	private int PIR_TRUE_WINDOW = 6;
	private LinkedList<Reading> sensorReadings;
	private String timestamp;

	public SensorReading()
	{
		sensorReadings = new LinkedList<Reading>();
		timestamp = Oscilloscope.getDateTime();
		windowPIR = new HashMap<String, Integer>();
		store = new HashMap<String, Boolean>();
		initStore();
	}

	public SensorReading(LinkedList<Reading> sensorReadings, String timestamp) {
		super();
		this.sensorReadings = new LinkedList<Reading>();
		this.timestamp = timestamp;
		this.store = new HashMap<String, Boolean>();
		Iterator<Reading> i = sensorReadings.iterator();
		while (i.hasNext())
		{
			Reading current = i.next();
			this.sensorReadings.add(new Reading(current.sensID, current.value));
		}
		
	}

	private void initStore()
	{
		store.put("1LCD", false);
		store.put("1PC", false);
		store.put("1Pressure", false);
		store.put("1PIR", false);
		
		store.put("2LCD", false);
		store.put("2PC", false);
		store.put("2Acoustic1", false);
		store.put("2Acoustic2", false);
		store.put("2PIR", false);
		store.put("2Projector", false);
		store.put("2Pressure1", false);
		store.put("2Pressure2", false);
		store.put("2Pressure3", false);
		
		store.put("3CoffeeMachine", false);
		store.put("3Microwave", false);
		store.put("3PIR", false);
		
		/* Initialize the store to set default value for all
		 * sensID. 
		 */
	}

	public synchronized String toString()
	{
		return new Gson().toJson(this);
	}


	public synchronized int push(String sensID, boolean value)
	{
		// Check for PIR
		if (sensID.matches(".*PIR.*"))
		{
			if (windowPIR.get(sensID) == null)
				windowPIR.put(sensID, 0);
			
			if (value)
			{
				windowPIR.put(sensID, PIR_TRUE_WINDOW);
				System.out.println("Start TRUE win for " + sensID);
			}
			
			if (!value && windowPIR.get(sensID) > 0)
			{
				System.out.println("Dropping " + sensID + " (" + value + ") reading due trueWindow");
				return -1;
			}
		}
		store.put(sensID, value);
		return __push(sensID, value);
	}

	private synchronized int __push(String sensID, boolean value)
	{
		
		int len = sensorReadings.size();
		for (int i = 0; i < len; i++)
		{
			Reading current = sensorReadings.get(i);
			if (sensID.compareToIgnoreCase(current.sensID) < 0)
			{
				sensorReadings.add(i, new Reading(sensID, value));
				return 1;
			} else
			{
				if (current.sensID.equalsIgnoreCase(sensID))
				{
					if (current.value || !value)
					{
						System.out.println("*** Drop second reading for " + sensID + " ***");
						return -1;
					} else
					{
						System.out.println("*** Substitude " + current.sensID + " with new one ***");
						sensorReadings.remove(i);
						sensorReadings.add(i, new Reading(sensID, value));
						return 1;
					}
				}
			}
		}
		sensorReadings.addLast(new Reading(sensID, value));
		return 1;
	}


	private synchronized void decreasePIRWindows()
	{
		Iterator<String> i = windowPIR.keySet().iterator();
		while (i.hasNext())
		{
			String current = i.next();
			int x = windowPIR.get(current);
			windowPIR.put(current, --x);
		}
	}
	
	public synchronized String flush(String timestamp)
	{
		decreasePIRWindows();
		Iterator<String> i = store.keySet().iterator();
		while (i.hasNext())
		{
			String current_key = i.next();
			boolean found = false;
			Iterator<Reading> j = sensorReadings.iterator();
			while (j.hasNext())
			{
				if (j.next().sensID.equalsIgnoreCase(current_key))
					found = true;
			}
			if (!found)
				this.__push(current_key, store.get(current_key));
		}
		this.timestamp = timestamp;
		String result = this.toString();
		//System.out.println(result);
		sensorReadings.clear();
		String ret = result.replaceFirst("\\{\"store\":.*sensorReadings", "");
		return "{\"sensorReadings" +ret;

	}

	public synchronized String toJson()
	{
		String result = this.toString();
		String ret = result.replaceFirst("\\{\"store\":.*sensorReadings", "");
		return "{\"sensorReadings" + ret;
	}
	
	public synchronized SensorReading clone()
	{
		return new SensorReading(this.sensorReadings, this.timestamp);
	}
	
	public synchronized LinkedList<Reading> getReadingList()
	{
		return this.sensorReadings;
	}
	
	public synchronized SensorReading flushClone(String timestamp)
	{
		decreasePIRWindows();
		Iterator<String> i = store.keySet().iterator();
		while (i.hasNext())
		{
			String current_key = i.next();
			boolean found = false;
			Iterator<Reading> j = sensorReadings.iterator();
			while (j.hasNext())
			{
				if (j.next().sensID.equalsIgnoreCase(current_key))
					found = true;
			}
			if (!found)
				this.__push(current_key, store.get(current_key));
		}
		this.timestamp = timestamp;
		SensorReading result = this.clone();
		sensorReadings.clear();
		return result;

	}
	public static void main(String[] arg)
	{
		SensorReading r = new SensorReading();
		r.push("sens1", true);
		r.push("sens3", false);
		System.out.println(r.flush(Oscilloscope.getDateTime()));
		r.push("sens2", false);
		r.push("sens0", false);
		r.push("Sens0", true);
		r.push("Asd", true);
		r.push("Sens0", true);
		System.out.println(" << " + r);
		System.out.println(r.sensorReadings.get(0).sensID);
		System.out.println(">> " + r.flush(Oscilloscope.getDateTime()));
	}





}
