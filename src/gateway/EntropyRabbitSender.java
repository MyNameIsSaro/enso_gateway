package gateway;
import com.google.gson.Gson;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import java.util.*;


public class EntropyRabbitSender
{
	
	private String exchange_name;
	private Channel channel;
	private Connection connection;
	private int errorThreshold;
	private Random rnd;
	
	public EntropyRabbitSender(String ex, int errorTh)
	{
		this.exchange_name = ex;
		this.errorThreshold = errorTh;
		this.rnd = new Random();
	}
	
	/**
	 * Method to create a new queue on RabbitMQ server.
	 */
	private void createChannel() throws java.io.IOException
	{
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("129.125.51.160");
		//factory.setHost("localhost");
		connection = factory.newConnection();
		channel = connection.createChannel();

		//channel.queueDeclare(queue_name, false, false, false, null);
	}
	
	/**
	 * Method to close the channel.
	 */
	private void closeChannel() throws java.io.IOException
	{
		channel.close();
		connection.close();
	}
	
	/**
	 * Method to send a message through RabbitMQ to the upper level of the architecture
	 * 
	 * @param map HashMap containing sensors state.
	 * @return true if the message is sent correctly, false otherwise.
	 */
	public boolean sendMessage(SensorReading buf)
	{
		try
		{
			// Process random phase
			Iterator<SensorReading.Reading> i = buf.getReadingList().iterator();
			while (i.hasNext())
			{
				SensorReading.Reading current = i.next();
				if (rnd.nextInt(100) < errorThreshold)
				{
					// Flip true/false sensor value
					System.out.println("* Flipping " + current.sensID + " *");
					if (current.value)
						current.value = false;
					else
						current.value = true;
				}
			}
			
			createChannel();
			channel.exchangeDeclare(exchange_name, "fanout"); 
			channel.basicPublish(exchange_name, "", null, buf.toJson().getBytes());
			System.out.println(" [Entropy]  '" + buf.toJson()  + "'");
			closeChannel();
			return true;
		} catch (java.io.IOException e)
		{
			e.printStackTrace();
			return false;
		}
	}
}
