package gateway;

import wsn.*;
import java.util.*;

/**
 * This class is the unique gateway for communicate WSN and Plugwise networks data to
 * the activity recognition systems through RabbitMQ
 * 
 * @author Rosario Contarino <contarino.rosario@gmail.com>
 *
 */

public class Gateway implements Runnable{

	WSNThread wsnT;
	PlugwiseThread plugwiseT;
	RabbitSender rmqSender;
	EntropyRabbitSender entropyRmqSender;
	//RabbitReceiver rmqReceiver;
	SensorReading sensBuffer;
	double THRESHOLD = 0.006;

	HashMap<String, String> plugwiseMap;


	public Gateway(String serialWSN, String serialPlugwise)
	{
		sensBuffer = new SensorReading();
		wsnT = new WSNThread(sensBuffer, serialWSN);
		plugwiseMap = new HashMap<String, String>();
		plugwiseMapping();
		plugwiseT = new PlugwiseThread(sensBuffer, serialPlugwise, plugwiseMap, THRESHOLD);
		rmqSender = new RabbitSender("logOri");
		entropyRmqSender = new EntropyRabbitSender("logError", 15);
		//rmqReceiver = new RabbitReceiver(plugwiseMap, plugwiseT.plugwise);

		Thread t = new Thread(this);
		t.start();
	}

	/**
	 * Link an plugwise MAC address with its sensID
	 */
	private void plugwiseMapping()
	{
		plugwiseMap.put("1LCD", "B81096");
		plugwiseMap.put("1PC", "B7BED0");
		
		plugwiseMap.put("2PC", "B83221");
		plugwiseMap.put("2LCD", "B83228");
		plugwiseMap.put("2Projector", "B82F6C");
		
		plugwiseMap.put("3CoffeeMachine", "B810FA");
		plugwiseMap.put("3Microwave", "98C0A0");
	}

	public static void main(String[] args) throws Exception 
	{
		if (args.length < 2)
		{
			System.out.println("Usage: rabbitGW <serialPortWSN> <serialPortPlugwise>");
			return;
		}
			
		new Gateway(args[0], args[1]);
	}

	/**
	 * Every 6 seconds the gateway flush its sensors readings queue in a 
	 * json message to send through RabbitMQ.
	 */
	@Override
	public void run() {
		try {
			//while(true)
			while(true)
			{
				//System.out.println("Begin!");
				Thread.sleep(12000);
				SensorReading buf = sensBuffer.flushClone(Oscilloscope.getDateTime());
				rmqSender.sendMessage(buf.toJson());
				entropyRmqSender.sendMessage(buf);
				
				//rmqSender.sendMessage(sensBuffer.flush(Oscilloscope.getDateTime()));
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}