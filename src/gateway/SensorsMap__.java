package gateway;

import java.util.*;

public class SensorsMap__ {
	
	public SensorsMap__()
	{
		map = new HashMap<String, InnerData>();
	}
	
	private HashMap<String, InnerData> map;
	
	private class InnerData
	{
		private boolean value;
		private String timestamp;
		
		public InnerData(boolean value, String timestamp)
		{
			this.value = value;
			this.timestamp = timestamp;
		}
		
		public InnerData(boolean value)
		{
			this.value = value;
			this.timestamp = null;
		}
		
		public String toString()
		{
			return "{\"value:\"" + value + ",\"timestamp\":" + timestamp + "}"; 
		}
	}
	
	public synchronized InnerData put(String key, boolean value, String time)
	{
		return map.put(key,new InnerData(value, time));
	}
	
	public synchronized InnerData put(String key, boolean value)
	{
		return map.put(key, new InnerData(value));
	}
	
	public synchronized void putAll(HashMap<String, InnerData> otherMap)
	{
		map.putAll(otherMap);
	}
	
	public synchronized String toString()
	{
		return map.toString();
	}
	
	public synchronized HashMap<String, InnerData>getMap()
	{
		return map;
	}
	
	public synchronized InnerData get(String key)
	{
		return map.get(key);
	}
	
	public synchronized boolean getValue(String key)
	{
		return map.get(key).value;
	}
	
	public synchronized String getTimeStamp(String key)
	{
		return map.get(key).timestamp;
	}
}
