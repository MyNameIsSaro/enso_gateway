package gateway;

public class CommandMsg {
	private String cmd;
	private boolean value;
	private String item;
	
	public String getCmd() {
		return cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public boolean isOn() {
		return value;
	}

	public void setValue(boolean value) {
		this.value = value;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}
	
	public CommandMsg(String cmd, String item, boolean value)
	{
		this.cmd = cmd;
		this.value = value;
		this.item = item;
	}
	
	public String toString()
	{
		return "[CMD:" + cmd + " on " + item + " : " + value + "]";
	}
	

}
