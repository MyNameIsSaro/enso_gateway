package gateway;
import com.google.gson.Gson;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import java.util.*;


public class RabbitSender
{
	
	private String exchange_name;
	private Channel channel;
	private Connection connection;
	
	public RabbitSender(String ex)
	{
		this.exchange_name = ex;
	}
	
	/**
	 * Method to create a new queue on RabbitMQ server.
	 */
	private void createChannel() throws java.io.IOException
	{
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("129.125.51.160");
		//factory.setHost("localhost");
		connection = factory.newConnection();
		channel = connection.createChannel();

		//channel.queueDeclare(queue_name, false, false, false, null);
	}
	
	/**
	 * Method to close the channel.
	 */
	private void closeChannel() throws java.io.IOException
	{
		channel.close();
		connection.close();
	}
	
	/**
	 * Method to send a message through RabbitMQ to the upper level of the architecture
	 * 
	 * @param map HashMap containing sensors state.
	 * @return true if the message is sent correctly, false otherwise.
	 */
	public boolean sendMessage(HashMap<String,Boolean> map)
	{
		Gson gson = new Gson();
		String json = gson.toJson(map);
		try
		{
			createChannel();
			channel.basicPublish("", exchange_name, null, json.getBytes());
			System.out.println(" [x] Sent '" + json  + "'");
			closeChannel();
			return true;
		} catch (java.io.IOException e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean sendMessage(String json)
	{

		try
		{
			createChannel();
			
			channel.exchangeDeclare(exchange_name, "fanout"); 
			channel.basicPublish(exchange_name, "", null, json.getBytes());
			
			System.out.println(" [x] Sent '" + json  + "'");
			closeChannel();
			return true;
		} catch (java.io.IOException e)
		{
			e.printStackTrace();
			return false;
		}
	}
		
}
