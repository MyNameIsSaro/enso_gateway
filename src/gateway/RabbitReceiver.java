package gateway;

import java.util.HashMap;
import plugwise.core.*;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.QueueingConsumer;
import com.google.gson.Gson;

public class RabbitReceiver implements Runnable {

	private final static String QUEUE_NAME = "queue_ENSO_cmd";
	HashMap<String, String> plugMap;
	PlugwiseWrapper plugwise;
	
	public RabbitReceiver(HashMap<String, String> plugMap, PlugwiseWrapper plugwise)
	{
		this.plugMap = plugMap;
		this.plugwise = plugwise;
		Thread t = new Thread(this);
		t.start();
	}
	
	private void execCommand(CommandMsg msg)
	{
		try
		{
			String MAC = plugMap.get(msg.getItem());
			plugwise.forcePlugState(MAC, msg.isOn());
		} catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("[ERROR] in execution of " + msg);
		}
	}

	@Override
	public void run() {

		try
		{

			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost("localhost");
			Connection connection = factory.newConnection();
			Channel channel = connection.createChannel();

			channel.queueDeclare(QUEUE_NAME, false, false, false, null);

			QueueingConsumer consumer = new QueueingConsumer(channel);
			channel.basicConsume(QUEUE_NAME, true, consumer);

			while (true) {
				QueueingConsumer.Delivery delivery = consumer.nextDelivery();
				String message = new String(delivery.getBody());

				System.out.println(" [x] Received '" + message + "'");        
				//HashMap<String, Boolean> map = new Gson().fromJson(message, HashMap.class);
				CommandMsg msg = new Gson().fromJson(message, CommandMsg.class);
				execCommand(msg);
				System.out.println(" [x] Done: " + msg.toString());
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}



}
