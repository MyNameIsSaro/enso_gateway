package wsn;

import java.io.*;

public class Logger {

	String filename;
	String ID;
	FileOutputStream file;
	PrintStream output;
	
	public Logger(String filename)
	{
		this.filename = filename;
		try
		{
			this.file = new FileOutputStream(filename, true);
			this.output = new PrintStream(file);
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}

	}
	
	public void log(String str)
	{
		output.println(str);
	}
	
	public void close()
	{
		output.close();
		try
		{
			file.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
