package wsn;

/*
 * Copyright (c) 2006 Intel Corporation
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached INTEL-LICENSE     
 * file. If you do not find these files, copies can be found by writing to
 * Intel Research Berkeley, 2150 Shattuck Avenue, Suite 1300, Berkeley, CA, 
 * 94704.  Attention:  Intel License Inquiry.
 */
// package net.tinyos.mesage;

import net.tinyos.message.*;
import net.tinyos.packet.BuildSource;
import net.tinyos.util.*;
import gateway.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.io.IOException;

import java.util.Date;


/* The "Oscilloscope" demo app. Displays graphs showing data received from
   the Oscilloscope mote application, and allows the user to:
   - zoom in or out on the X axis
   - set the scale on the Y axis
   - change the sampling period
   - change the color of each mote's graph
   - clear all data

   This application is in three parts:
   - the Node and Data objects store data received from the motes and support
     simple queries
   - the Window and Graph and miscellaneous support objects implement the
     GUI and graph drawing
   - the Oscilloscope object talks to the motes and coordinates the other
     objects

   Synchronization is handled through the Oscilloscope object. Any operation
   that reads or writes the mote data must be synchronized on Oscilloscope.
   Note that the messageReceived method below is synchronized, so no further
   synchronization is needed when updating state based on received messages.
 */
public class Oscilloscope implements MessageListener
{
	MoteIF mote;
	Data data;
	int maxValue, minValue;
	float PressureAverageValue;
	float PIRMaxValue;
	float MicAverageValue;
	int nodeid;
	int counterPIR = 0;
	int counterPressure = 0;
	int counterAcoustic = 0;

	float[] AcousticSamples = new float[600];
	float AcousticValue;

	int[] CopyArray = new int[10];
	Logger lightLog;
	//int counterAcoustic;
	//boolean PIRStatus = false;
	//boolean WorkingChairStatus = false;
	//boolean MeetingChairStatus = false;
	//boolean RelaxChairStatus = false;
	//boolean AcousticStatus = false;
	//SensorsMap sensorsMap;
	SensorReading sensBuffer;
	String serialPort = "/dev/ttyUSB0"; 

	//	boolean PIRSynch = false;
	//	boolean PressureSynch = false;
	//	boolean AcousticSynch = false;


	String Time;
	//boolean PIRStatus, PressureStatus, AcousticStatus;
	// Calendar cal = Calendar.getInstance();
	// DateFormat df = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.MEDIUM);

	/* The current sampling period. If we receive a message from a mote
       with a newer version, we update our interval. If we receive a message
       with an older version, we broadcast a message with the current interval
       and version. If the user changes the interval, we increment the
       version and broadcast the new interval and version. */
	int interval = Constants.DEFAULT_INTERVAL;
	int version = -1;

	// Print time function
	public static String getDateTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}

	/**
	 * Oscilloscope Constructor.
	 */
	public Oscilloscope(SensorReading sensBuffer, String serialPort)
	{
		System.out.println("Starting Run");
		data = new Data(this);
		System.out.println("Before creating mote");
		//mote = new MoteIF(PrintStreamMessenger.err);
		mote = new MoteIF(BuildSource.makePhoenix("serial@" + serialPort + ":115200", PrintStreamMessenger.err));
		System.out.println("serial@" + serialPort + ":115200");
		System.out.println("Before mote.registerListener");
		mote.registerListener(new OscilloscopeMsg(), this);
		System.out.println("Here!");
		//sensorsMap = map;
		this.sensBuffer = sensBuffer; 
		lightLog = new Logger("lightLog.log");
	}


	public synchronized void messageReceived(int dest_addr, Message msg)  {
		//System.out.println("I am inside messageReceived");
		if (msg instanceof OscilloscopeMsg) {
			//System.out.println("Function Oscilloscope.messageReceived");
			OscilloscopeMsg omsg = (OscilloscopeMsg)msg;

			/* Update interval and mote data */
			periodUpdate(omsg.get_version(), omsg.get_interval());
			data.update(omsg.get_id(), omsg.get_count(), omsg.get_readings());
			nodeid = omsg.get_id();
			System.out.println("Node ID: " + nodeid + " Value " + data.maxX(nodeid));
			//		Recognition(nodeid);
			//System.out.println("PIR Status = " + PIRStatus + "; Pressure Status = " + PressureStatus + "; Acoustic Status = " + AcousticStatus);
			
			switch(nodeid)
			{
				case 1:
					sensBuffer.push("1PIR", PIRStatusUpdate(nodeid));
					break;
					
				case 2:
					sensBuffer.push("1Pressure", PressureStatusUpdate(nodeid));
					break;
					
				case 3:
					sensBuffer.push("2Acoustic1", AcousticStatusUpdate(nodeid));
					break;
					
				case 4:
					sensBuffer.push("2Acoustic2", AcousticStatusUpdate(nodeid));
					break;

				case 5:
					sensBuffer.push("2PIR", PIRStatusUpdate(nodeid));
					break;
					
				case 6:
					sensBuffer.push("2Pressure1", PressureStatusUpdate(nodeid));
					break;
					
				case 7:
					sensBuffer.push("2Pressure2", PressureStatusUpdate(nodeid));
					break;	
				
				case 8:
					sensBuffer.push("2Pressure3", PressureStatusUpdate(nodeid));
					break;	
					
				case 9:
					sensBuffer.push("3PIR", PIRStatusUpdate(nodeid));
					break;
					
				case 20:
					boolean res[] = getPIRMIC(nodeid, omsg.get_readings());
					sensBuffer.push("TestPIR", res[0]);
					sensBuffer.push("TestMIC", res[1]);
					break;
				
				case 30:
					logLightStatus(nodeid, lightLog);
					break;
					
				default:
					System.out.println("# Node : " + nodeid + "expected! #");
			}
			
			
			
			/*
			if (nodeid == 2){
				//WorkingChairStatus = PressureStatusUpdate(nodeid);
				//			System.out.println("WorkingChairStatus:" + WorkingChairStatus);
				//sensorsMap.put("WorkingChair", WorkingChairStatus, getDateTime());
				sensBuffer.push("WorkingChair", WorkingChairStatus);
			}
			if(nodeid == 3){
				MeetingChairStatus = PressureStatusUpdate(nodeid);
				//			System.out.println("MeetingChairStatus:" + MeetingChairStatus);
				//sensorsMap.put("MeetingChair", MeetingChairStatus, getDateTime());
				sensBuffer.push("MeetingChair", MeetingChairStatus);
			}
			if (nodeid == 6){
				RelaxChairStatus = PressureStatusUpdate(nodeid);
				//			System.out.println("RelaxChairStatus:" + RelaxChairStatus);
				sensBuffer.push("RelaxChair", RelaxChairStatus);
			}

			if (nodeid == 4){
				AcousticStatus = AcousticStatusUpdate(nodeid);
				//			System.out.println("AcousticStatus:" + AcousticStatus);
				sensBuffer.push("Acoustic", AcousticStatus);
			}		

			if (nodeid == 5)
			{
				PIRStatus = PIRStatusUpdate(nodeid);
				System.out.println(PIRStatus);
				sensBuffer.push("PIR", PIRStatus);
				Time = getDateTime();
				System.out.print(Time + ": ");
				System.out.print("PIR = " + PIRStatus + "; Working = " + WorkingChairStatus + "; Meeting = " + MeetingChairStatus);
				System.out.println("; Relax = " + RelaxChairStatus + "; Acoustic = " + AcousticStatus);
			}*/

		}
	}


	/* A potentially new version and interval has been received from the
       mote */
	void periodUpdate(int moteVersion, int moteInterval) {
		// System.out.println("Function Oscilloscope.periodUpdate");
		if (moteVersion > version) {
			/* It's new. Update our vision of the interval. */
			version = moteVersion;
			interval = moteInterval;
		}
		else if (moteVersion < version) {
			/* It's old. Update the mote's vision of the interval. */
			sendInterval();
		}
	}

	/* The user wants to set the interval to newPeriod. Refuse bogus values
       and return false, or accept the change, broadcast it, and return
       true */
	synchronized boolean setInterval(int newPeriod) {
		if (newPeriod < 1 || newPeriod > 65535) {
			return false;
		}
		interval = newPeriod;
		version++;
		sendInterval();
		return true;
	}

	/* Broadcast a version+interval message. */
	void sendInterval() {
		System.out.println("*** SENDING INTERVALL ***");
		OscilloscopeMsg omsg = new OscilloscopeMsg();

		omsg.set_version(version);
		omsg.set_interval(interval);
		try {
			mote.send(MoteIF.TOS_BCAST_ADDR, omsg);
		}
		catch (IOException e) {
			System.out.println("Cannot send message to mote");
		}
	}

	/* User wants to clear all data. */
	void clear() {
		data = new Data(this);
	}

	/* New reading */
	
	boolean[] getPIRMIC(int nodeId, int readings[])
	{
		boolean result[]  = new boolean[2];
		
		if (readings[0] == 0) // Read PIR
			if (readings[1] > 2000)
				result[0] = true;
			else
				result[0] = false;
		if (readings[2] == 1) // Read MIC
			if (readings[3] >= 2500)
				result[1] = true;
			else
				result[1] = false;
		System.out.println("NODE:" + nodeId + " PIR:" + result[0] + " MIC:" + readings[3]);
		return result;
	}
	
	
	/* Tuan Anh Nguyen functions */
	boolean PIRStatusUpdate(int nodeId) {
		float maxValue = data.maxX(nodeId);
		// System.out.println("PIR Max value: " + maxValue);
		if (maxValue >=2500) { return true;}
		else return false;
	}

	boolean PressureStatusUpdate(int nodeId) {
		float maxValue = data.maxX(nodeId);
		// System.out.println("PIR Max value: " + maxValue);
		if (maxValue >= 10) { return true;}
		else return false;
	}

	boolean AcousticStatusUpdate(int nodeId) {
		//System.out.println("Acoustic average value = " + averValue + "; Acoustic Max = " + maxValue + "; Check value = " + checkValue);
		float maxValue = data.maxX(nodeId);
		// System.out.println("PIR Max value: " + maxValue);
		if (maxValue >=2500) { return true;}
		else return false;
	}
	
	boolean LightStatusUpdate(int nodeId) {
		//System.out.println("Acoustic average value = " + averValue + "; Acoustic Max = " + maxValue + "; Check value = " + checkValue);
		float maxValue = data.maxX(nodeId);
		// System.out.println("PIR Max value: " + maxValue);
		if (maxValue >=2500)
			return true;
		else
			return false;
	}

	void logLightStatus(int nodeId, Logger log)
	{
		if (data.maxX(nodeId) == 255)
			log.log("[" + Oscilloscope.getDateTime() + "] Lights turned OFF");
		if (data.maxX(nodeId) == 4095)
			log.log("[" + Oscilloscope.getDateTime() + "] Lights turned ON");
	}
	public static void main(String[] args) {
		//System.out.println("Starting program");
		//SensorsMap map = new SensorsMap();
		SensorReading buf = new SensorReading();
		Oscilloscope me = new Oscilloscope(buf, "/dev/ttyUSB0");
		//   ClassExecutingTask executingTask = new ClassExecutingTask();
		//   int activity = executingTask.start();
		//	switch (activity)
		//	{
		//		case 1: System.out.println("Working");
		//		case 2: System.out.println("Meeting");
		//		case 3: System.out.println("Relaxing");
		//		case 4: System.out.println("Presence");
		//		case 5: System.out.println("Absence");
		//		break;
		//	}

	}
}
