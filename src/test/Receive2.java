package test;


import gateway.*;

import java.util.HashMap;


import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.QueueingConsumer;
import com.google.gson.Gson;

public class Receive2 {
	
	private final static String QUEUE_NAME = "queue_ENSO";
	
	private static void doWork(String task) throws InterruptedException {
	    for (char ch: task.toCharArray()) {
	        if (ch == '.') Thread.sleep(1000);
	    }
	}
	/**
	 * @param args
	 */
	
	public static void main(String[] args) throws java.io.IOException,
    java.lang.InterruptedException {
		// TODO Auto-generated method stub
		
		
		ConnectionFactory factory = new ConnectionFactory();
	    factory.setHost("localhost");
	    Connection connection = factory.newConnection();
	    Channel channel = connection.createChannel();

	    channel.queueDeclare(QUEUE_NAME, false, false, false, null);
	    System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
	    
	    QueueingConsumer consumer = new QueueingConsumer(channel);
	    channel.basicConsume(QUEUE_NAME, true, consumer);

	    while (true) {
	      QueueingConsumer.Delivery delivery = consumer.nextDelivery();
	      String message = new String(delivery.getBody());
	            
	      System.out.println(" [x] Received '" + message + "'");        
	      //HashMap<String, Boolean> map = new Gson().fromJson(message, HashMap.class);
	      SensorReading buff = new Gson().fromJson(message, SensorReading.class);
	      System.out.println(" [x] Done: " + buff.toString());
	    }
	}

}
