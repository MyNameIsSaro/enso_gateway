package test;

import java.io.*;

public class ResponseParser {

	public static PlugStatus parsePlugLivepower(BufferedReader reader)
	{
		String line;
		String output = "";
		PlugStatus status;
		String mac = "000000";
		float value = 0;
		float value8 = 0;
		String units = "kW";
		try
		{
			while ((line = reader.readLine()) != null)
			{
				output += (line + '\n');
				if (line.matches(".*'device',$"))
				{
					mac = reader.readLine().replaceAll("\\W", "");
					System.out.println("MAC:" + mac);
				}
				if (line.matches(".*'current',$"))
				{
					value = Float.parseFloat(reader.readLine().replaceAll("\\W", ""));
					System.out.println("current:" + value);
				}
				if (line.matches(".*'current8',$"))
				{
					value8 = Float.parseFloat(reader.readLine().replaceAll("\\W", ""));
					System.out.println("current8:" + value8);
				}
				if (line.matches(".*'units',$"))
				{
					units = reader.readLine().replaceAll("\\W", "");
					System.out.println("units:" + units);
				}
			}
		return new PlugStatus(mac, value, value8, units); 
		} catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try{
			  // Open the file that is the first 
			  // command line parameter
			  FileInputStream fstream = new FileInputStream("/home/saro/plug_out_livepower");
			  // Get the object of DataInputStream
			  DataInputStream in = new DataInputStream(fstream);
			  BufferedReader br = new BufferedReader(new InputStreamReader(in));
			  System.out.println(ResponseParser.parsePlugLivepower(br));
		} catch (Exception e)
		{
			e.printStackTrace();
		}

	}

}
