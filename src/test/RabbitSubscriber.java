package test;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.QueueingConsumer;

public class RabbitSubscriber implements Runnable{

	String ex;
	
	
	public RabbitSubscriber(String ex) {
		this.ex = ex;
		Thread t = new Thread(this);
		t.start();
	}
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		RabbitSubscriber s1 = new RabbitSubscriber("logError");
		//RabbitSubscriber s2 = new RabbitSubscriber("log");
	}

	@Override
	public void run() {
		try {
		ConnectionFactory factory = new ConnectionFactory();
        //factory.setHost("localhost");
        factory.setHost("129.125.51.160");
		
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(ex, "fanout");
        String queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, ex, "");

        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        QueueingConsumer consumer = new QueueingConsumer(channel);
        channel.basicConsume(queueName, true, consumer);

        while (true) {
            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
            String message = new String(delivery.getBody());

            System.out.println(message);
        }
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
