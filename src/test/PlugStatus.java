package test;
import com.google.gson.*;


public class PlugStatus {
	public PlugStatus(String mac, float currentPower, float current8Power,
			String units) {
		super();
		this.mac = mac;
		this.currentPower = currentPower;
		this.current8Power = current8Power;
		this.units = units;
	}
	public String mac;
	public float currentPower;
	public float current8Power;
	public String units;
	
	public String toString()
	{
		return new Gson().toJson(this);
	}
}
