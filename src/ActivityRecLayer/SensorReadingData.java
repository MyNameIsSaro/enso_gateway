package ActivityRecLayer;

import com.google.gson.*;

import java.util.*;

import wsn.Oscilloscope;

public class SensorReadingData 
{
	public class Reading
	{
		public String sensID;
		public boolean value;
		
		public Reading(String sensID, boolean value)
		{
			this.sensID = sensID;
			this.value = value;
		}
	}
	
	public LinkedList<Reading> sensorReadings;
	public String timestamp;
	
	public SensorReadingData()
	{
		sensorReadings = new LinkedList<Reading>();
		timestamp = Oscilloscope.getDateTime();
	}
	
	public SensorReadingData(String json)
	{
		this();
		SensorReadingData r = new Gson().fromJson(json, SensorReadingData.class);
		this.sensorReadings.addAll(r.sensorReadings);
		this.timestamp = r.timestamp;
		
	}
	
	public int add(String sensID, boolean value)
	{
		int len = sensorReadings.size();
		for (int i = 0; i < len; i++)
		{
			Reading current = sensorReadings.get(i);
			if (sensID.compareToIgnoreCase(current.sensID) < 0)
			{
				sensorReadings.add(i, new Reading(sensID, value));
				return 1;
			}
		}
		sensorReadings.addLast(new Reading(sensID, value));
		return 1; 
	}
	
	public LinkedList<Reading> getReadings()
	{
		return this.sensorReadings;
	}
	
	public String getTimestamp()
	{
		return this.timestamp;
	}
	
	public synchronized String toString()
	{
		return new Gson().toJson(this);
	}
	
}
