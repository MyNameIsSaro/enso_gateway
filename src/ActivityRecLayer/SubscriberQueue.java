package ActivityRecLayer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;


public class SubscriberQueue {

	private LinkedList<SensorReadingData> queue;
	private String lastTimestamp;
	
	public static String getDateTime()
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}
	
	public SubscriberQueue()
	{
		queue = new LinkedList<SensorReadingData>();
	}
	
	public boolean push(SensorReadingData data)
	{
		lastTimestamp = data.timestamp;
		return queue.add(data);
	}
	
	public SensorReadingData getAverageData()
	{
		// Collect data in valuesMap
		
		HashMap<String, LinkedList<Boolean>> valuesMap = new HashMap<String, LinkedList<Boolean>>();
		Iterator<SensorReadingData> iData = queue.iterator();
		while (iData.hasNext())
		{
			SensorReadingData currentData = iData.next();
			Iterator<SensorReadingData.Reading> jRead = currentData.sensorReadings.iterator();
			while (jRead.hasNext())
			{
				SensorReadingData.Reading currentRead = jRead.next();
				
				if (valuesMap.get(currentRead.sensID) == null)
					valuesMap.put(currentRead.sensID, new LinkedList<Boolean>());
				
				valuesMap.get(currentRead.sensID).add(currentRead.value);
					
			}
		}
		
		System.out.println(valuesMap);
		
		// Process collected data
		int len = queue.size();
		SensorReadingData newData = new SensorReadingData();
		Iterator<String> iKey = valuesMap.keySet().iterator();
		while (iKey.hasNext())
		{
			String currentKey = iKey.next();
			Iterator<Boolean> jValues = valuesMap.get(currentKey).iterator();
			int trueCount = 0;
			while (jValues.hasNext())
			{
				if (jValues.next())
					trueCount++;
			}
			if (trueCount >= ((int)(len / 2) + (len % 2)))
				newData.add(currentKey, true);
			else
				newData.add(currentKey, false);
				
		}
		newData.timestamp = this.lastTimestamp;
		
		queue.clear();
		
		return newData;
	}
		
		
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
