package ActivityRecLayer;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.QueueingConsumer;

public class RabbitSubscriber implements Runnable{

	String ex;
	SubscriberQueue queue;
	
	
	public RabbitSubscriber(String ex) {
		this.ex = ex;
		queue = new SubscriberQueue();
		Thread t = new Thread(this);
		t.start();
	}
	
	public SensorReadingData getAverageData()
	{
		return queue.getAverageData();
	}
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		RabbitSubscriber s1 = new RabbitSubscriber("log");
		//RabbitSubscriber s2 = new RabbitSubscriber("log");
		while (true) {
			try {
				Thread.sleep(10000);
			}catch(Exception e)
			{
				e.printStackTrace();
			}

			System.out.println(s1.getAverageData());
		}
		
	}

	@Override
	public void run() {
		try {
		ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(ex, "fanout");
        String queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, ex, "");

        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        QueueingConsumer consumer = new QueueingConsumer(channel);
        channel.basicConsume(queueName, true, consumer);

        while (true) {
            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
            String message = new String(delivery.getBody());

            //System.out.println(" [x] R '"+ this +"  " + message + "'");
            SensorReadingData data = new SensorReadingData(message);
            queue.push(data);
        }
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
