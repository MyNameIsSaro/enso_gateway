package plugwise.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "power")
@XmlAccessorType(XmlAccessType.FIELD)
public class Power {
	@XmlElement
	private String macID;
	@XmlElement
	private String deviceName;
	@XmlElement
	private double maxpower;
	public String getMacID() {
		return macID;
	}
	public void setMacID(String macID) {
		this.macID = macID;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public double getMaxpower() {
		return maxpower;
	}
	public void setMaxpower(double maxpower) {
		this.maxpower = maxpower;
	}
}
