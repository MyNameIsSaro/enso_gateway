package plugwise.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "device")
@XmlAccessorType(XmlAccessType.FIELD)
public class RepoDevice {
	@XmlElement
	private String macID;
	@XmlElement
	private String deviceName;
	@XmlElementWrapper(name = "states")
	@XmlElement(name = "state")
	private List<RepoState> states = new ArrayList<RepoState>();
	@XmlElement
	private int initialStateID;
	/**
	 * @return the macID
	 */
	public String getMacID() {
		return macID;
	}
	/**
	 * @param macID the macID to set
	 */
	public void setMacID(String macID) {
		this.macID = macID;
	}
	/**
	 * @return the deviceName
	 */
	public String getDeviceName() {
		return deviceName;
	}
	/**
	 * @param deviceName the deviceName to set
	 */
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	/**
	 * @return the initialStateID
	 */
	public int getInitialStateID() {
		return initialStateID;
	}
	/**
	 * @param initialStateID the initialStateID to set
	 */
	public void setInitialStateID(int initialStateID) {
		this.initialStateID = initialStateID;
	}
	/**
	 * @return the states
	 */
	public List<RepoState> getStates() {
		return states;
	}
	
	public void seStates(List<RepoState> states){
		this.states = states;
	}
	
	public void setState(RepoState state){
		this.states.add(state);
	}
	
}
