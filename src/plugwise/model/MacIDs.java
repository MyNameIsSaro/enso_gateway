package plugwise.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "macIDs")
@XmlAccessorType(XmlAccessType.FIELD)
public class MacIDs {

	@XmlElement(name = "macID")
	private List<String> macids = new ArrayList<String>();

	public List<String> getMacids() {
		return macids;
	}

	public void setMacids(List<String> macids) {
		this.macids = macids;
	}
}
