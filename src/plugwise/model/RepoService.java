package plugwise.model;

import java.util.List;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "service")
@XmlAccessorType(XmlAccessType.FIELD)
public class RepoService {
	@XmlElement
	private String serviceName;
	@XmlElementWrapper(name = "states")
	@XmlElement(name = "state")
	private List<RepoState> states = new ArrayList<RepoState>();
	/**
	 * @return the serviceName
	 */
	public String getServiceName() {
		return serviceName;
	}
	/**
	 * @param serviceName the serviceName to set
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	/**
	 * @return the states
	 */
	public List<RepoState> getStates() {
		return states;
	}
	/**
	 * @param states the states to set
	 */
	public void setStates(List<RepoState> states) {
		this.states = states;
	}
	
	public void setState(RepoState state){
		this.states.add(state);
	}
}
