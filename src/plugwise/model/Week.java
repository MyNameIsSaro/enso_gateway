package plugwise.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "week")
@XmlAccessorType(XmlAccessType.FIELD)
public class Week {
	@XmlElement
	private String macID;
	@XmlElement(name = "day")
	private List<Day> days = new ArrayList<Day>();

	public String getMacID() {
		return macID;
	}

	public void setMacID(String macID) {
		this.macID = macID;
	}

	public List<Day> getDays() {
		return days;
	}

	public void setDays(List<Day> days) {
		this.days = days;
	}
	
	public void setDay(Day day) {
		this.days.add(day);
	}
}
