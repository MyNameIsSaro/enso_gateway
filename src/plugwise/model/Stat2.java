package plugwise.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "stat")
@XmlAccessorType(XmlAccessType.FIELD)
public class Stat2 {
	@XmlElement
	private String timestamp;
	@XmlElement
	private double averageEnergyCost;
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public double getAverageEnergyCost() {
		return averageEnergyCost;
	}
	public void setAverageEnergyCost(double averageEnergyCost) {
		this.averageEnergyCost = averageEnergyCost;
	}
}
