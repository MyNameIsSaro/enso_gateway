package plugwise.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "policies")
@XmlAccessorType(XmlAccessType.FIELD)
public class Costs {
	@XmlElement(name = "policy")
	private List<Cost> costs = new ArrayList<Cost>();

	public List<Cost> getCosts() {
		return costs;
	}

	public void setCosts(List<Cost> costs) {
		this.costs = costs;
	}
	public void setCost(Cost c){
		this.costs.add(c);
	}

}
