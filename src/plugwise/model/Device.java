package plugwise.model;

import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name = "device")
@XmlAccessorType(XmlAccessType.FIELD)
public class Device {
	@XmlElement
	private String macID;
	@XmlElement
	private String deviceName;
	@XmlElement
	private int status;
	@XmlElement
	private String location;
	@XmlElement
	private double maxpower;
	//@XmlElementWrapper(name = "measures")
	//@XmlElement(name = "measure")
	@XmlTransient
	private Set<Measure> measures = new HashSet<Measure>();
	/**
	 * @return the macID
	 */
	public String getMacID() {
		return macID;
	}
	/**
	 * @param macID the macID to set
	 */
	public void setMacID(String macID) {
		this.macID = macID;
	}
	/**
	 * @return the deviceName
	 */
	public String getDeviceName() {
		return deviceName;
	}
	/**
	 * @param deviceName the deviceName to set
	 */
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public double getMaxpower() {
		return maxpower;
	}
	public void setMaxpower(double maxpower) {
		this.maxpower = maxpower;
	}
	/**
	 * @return the measures
	 */
	public Set<Measure> getMeasures() {
		return measures;
	}
	/**
	 * @param measures the measures to set
	 */
	public void setMeasures(Set<Measure> measures) {
		this.measures = measures;
	}
	public void setMeasure(Measure m){
		this.measures.add(m);
	}
	
}
