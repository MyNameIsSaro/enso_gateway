package plugwise.model;

import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "measures")
@XmlAccessorType(XmlAccessType.FIELD)
public class Measures {
	@XmlElement(name = "measure")
	private Set<Measure> measures = new HashSet<Measure>();

	/**
	 * @return the measures
	 */
	public Set<Measure> getMeasures() {
		return measures;
	}

	/**
	 * @param measures the measures to set
	 */
	public void setMeasures(Set<Measure> measures) {
		this.measures = measures;
	}
}
