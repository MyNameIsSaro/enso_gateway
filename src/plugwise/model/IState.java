package plugwise.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "istate")
@XmlAccessorType(XmlAccessType.FIELD)
public class IState {
	@XmlElement
	private String macID;
	@XmlElement
	private int stateID;
	public String getMacID() {
		return macID;
	}
	public void setMacID(String macID) {
		this.macID = macID;
	}
	public int getStateID() {
		return stateID;
	}
	public void setStateID(int stateID) {
		this.stateID = stateID;
	}
}
