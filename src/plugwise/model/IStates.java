package plugwise.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;



@XmlRootElement(name = "istates")
@XmlAccessorType(XmlAccessType.FIELD)
public class IStates {
	@XmlElement(name = "istate")
	private List<IState> states = new ArrayList<IState>();

	public List<IState> getStates() {
		return states;
	}

	public void setStates(List<IState> states) {
		this.states = states;
	}
	
	public void setState(IState state){
		this.states.add(state);
	}
}
