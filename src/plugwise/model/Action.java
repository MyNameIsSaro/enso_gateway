package plugwise.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name ="action")
@XmlAccessorType(XmlAccessType.FIELD)
public class Action {
	@XmlElement
	private String macID;
	@XmlElement
	private String stateID;
	/**
	 * @return the macID
	 */
	public String getMacID() {
		return macID;
	}
	/**
	 * @param macID the macID to set
	 */
	public void setMacID(String macID) {
		this.macID = macID;
	}
	/**
	 * @return the stateID
	 */
	public String getStateID() {
		return stateID;
	}
	/**
	 * @param stateID the stateID to set
	 */
	public void setStateID(String stateID) {
		this.stateID = stateID;
	}

}
