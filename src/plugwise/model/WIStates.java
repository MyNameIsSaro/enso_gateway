package plugwise.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "istates")
@XmlAccessorType(XmlAccessType.FIELD)
public class WIStates {
	@XmlElement(name = "istate")
	private List<WIState> states = new ArrayList<WIState>();

	public List<WIState> getStates() {
		return states;
	}

	public void setStates(List<WIState> states) {
		this.states = states;
	}
	
	public void setState(WIState state){
		this.states.add(state);
	}
}
