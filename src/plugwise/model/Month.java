package plugwise.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "month")
@XmlAccessorType(XmlAccessType.FIELD)
public class Month {
	@XmlElement
	private String monthNum;
	@XmlElement
	private double energyCost;
	public String getMonth() {
		return monthNum;
	}
	public void setMonth(String monthNum) {
		this.monthNum = monthNum;
	}
	public double getEnergyCost() {
		return energyCost;
	}
	public void setEnergyCost(double energyCost) {
		this.energyCost = energyCost;
	}
}
