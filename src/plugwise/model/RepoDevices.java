package plugwise.model;

import java.util.List;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name ="devices")
@XmlAccessorType(XmlAccessType.FIELD)
public class RepoDevices {
	@XmlElement(name = "device")
	private List<RepoDevice> devices = new ArrayList<RepoDevice>();

	/**
	 * @return the devices
	 */
	public List<RepoDevice> getDevices() {
		return devices;
	}

	/**
	 * @param devices the devices to set
	 */
	public void setDevices(List<RepoDevice> devices) {
		this.devices = devices;
	}
	
	public void setDevice(RepoDevice device){
		this.devices.add(device);
	}
}
