package plugwise.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "stats")
@XmlAccessorType(XmlAccessType.FIELD)
public class Stats {
	@XmlElement(name = "stat")
	private List<Stat> stats = new ArrayList<Stat>();

	public List<Stat> getStats() {
		return stats;
	}

	public void setStats(List<Stat> stats) {
		this.stats = stats;
	}
	
	public void setStat(Stat stat){
		this.stats.add(stat);
	}
}
