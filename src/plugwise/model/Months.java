package plugwise.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "months")
@XmlAccessorType(XmlAccessType.FIELD)
public class Months {
	@XmlElement
	private String year;
	@XmlElement(name = "month")
	private List<Month> months = new ArrayList<Month>();

	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	
	public List<Month> getMonths() {
		return months;
	}

	public void setMonths(List<Month> months) {
		this.months = months;
	}
	
	public void setMonth(Month month) {
		this.months.add(month);
	}
}
