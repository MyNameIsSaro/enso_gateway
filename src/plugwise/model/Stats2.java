package plugwise.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "stats")
@XmlAccessorType(XmlAccessType.FIELD)
public class Stats2 {
	@XmlElement(name = "stat")
	private List<Stat2> stats = new ArrayList<Stat2>();

	public List<Stat2> getStats() {
		return stats;
	}

	public void setStats(List<Stat2> stats) {
		this.stats = stats;
	}
	
	public void setStat2(Stat2 stat){
		this.stats.add(stat);
	}
}
