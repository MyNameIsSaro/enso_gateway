package plugwise.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "policy")
@XmlAccessorType(XmlAccessType.FIELD)
public class Cost {
	@XmlElement
	private String timeOn;
	@XmlElement
	private String timeOff;
	@XmlElement
	private double energyCost;
	
	public String getTimeOn() {
		return timeOn;
	}
	public void setTimeOn(String timeOn) {
		this.timeOn = timeOn;
	}
	public String getTimeOff() {
		return timeOff;
	}
	public void setTimeOff(String timeOff) {
		this.timeOff = timeOff;
	}
	public double getEnergyCost() {
		return energyCost;
	}
	public void setEnergyCost(double energyCost) {
		this.energyCost = energyCost;
	}
	
}
