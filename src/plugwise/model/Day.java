package plugwise.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "day")
@XmlAccessorType(XmlAccessType.FIELD)
public class Day {
	@XmlElement
	private String date;
	@XmlElement
	private String energyCost;
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getEnergyCost() {
		return energyCost;
	}
	public void setEnergyCost(String energyCost) {
		this.energyCost = energyCost;
	}

}
