package plugwise.model;

import java.sql.Timestamp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import plugwise.core.TimestampAdapter;

@XmlRootElement(name = "measure")
@XmlAccessorType(XmlAccessType.FIELD)
public class Measure {
	@XmlTransient
	private int id;
	@XmlElement
	private String macID;
	@XmlElement
	private int stateID = -1;
	@XmlElement
	private double energyCost;
	@XmlJavaTypeAdapter(TimestampAdapter.class)
	private Timestamp timestamp;
	@XmlTransient
	private Device device;
/*	@XmlTransient
	private Set<Device> devices = new HashSet<Device>();*/
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the macID
	 */
	public String getMacID() {
		return macID;
	}
	/**
	 * @param macID the macID to set
	 */
	public void setMacID(String macID) {
		this.macID = macID;
	}
	/**
	 * @return the stateID
	 */
	public int getStateID() {
		return stateID;
	}
	/**
	 * @param stateID the stateID to set
	 */
	public void setStateID(int stateID) {
		this.stateID = stateID;
	}
	/**
	 * @return the energyCost
	 */
	public double getEnergyCost() {
		return energyCost;
	}
	/**
	 * @param energyCost the energyCost to set
	 */
	public void setEnergyCost(double energyCost) {
		this.energyCost = energyCost;
	}
	/**
	 * @return the timestamp
	 */
	public Timestamp getTimestamp() {
		return timestamp;
	}
	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}
	/*public Set<Device> getDevices() {
		return devices;
	}
	public void setDevices(Set<Device> devices) {
		this.devices = devices;
	}
	
	public void setDevice(Device d){
		this.devices.add(d);
	}*/
	public Device getDevice() {
		return device;
	}
	public void setDevice(Device device) {
		this.device = device;
	}
	
}
