package plugwise.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import plugwise.model.Measure;
import plugwise.model.Measures;

public class PerlWrapper__ {
	private Process logger;
	private ProcessBuilder loggerbuilder;
	private String XPLLOGGER_path =	"/home/saro/Università/placement/iche/xpl-logger";

	public PerlWrapper__(){}

	public String executeAction(String macID, String stateID) throws Exception {

		System.out.println("[executeAction]: received " + macID + ", " + stateID);

		String line = "";

		loggerbuilder = new ProcessBuilder("perl",XPLLOGGER_path);
		loggerbuilder.redirectErrorStream(true);

		logger = loggerbuilder.start();
		Thread.sleep(100);

		ProcessBuilder builder = new ProcessBuilder("perl", "execute.pl", macID, stateID);
		builder.redirectErrorStream(true);

		Process perlproc = builder.start();

		perlproc.waitFor();
		//perlproc.destroy();	

		BufferedReader input = new BufferedReader(new InputStreamReader(logger.getInputStream()));
		line = getActionStatus(input);

		//System.out.println(macID + " RESPONSE " + line);

		logger.destroy();

		line = collectMeasure(macID, stateID);

		//System.out.println("GATHERED " + line);

		return line;
	}

	public String executeActionRecursive(String macID, String stateID) throws Exception {

		System.out.println("[executeAction]: received " + macID + ", " + stateID);

		String line = "";

		loggerbuilder = new ProcessBuilder("perl",XPLLOGGER_path);
		loggerbuilder.redirectErrorStream(true);

		logger = loggerbuilder.start();

		ProcessBuilder builder = new ProcessBuilder("perl", "execute.pl", macID, stateID);
		builder.redirectErrorStream(true);

		Process perlproc = builder.start();

		perlproc.waitFor();
		perlproc.destroy();	

		BufferedReader input = new BufferedReader(new InputStreamReader(logger.getInputStream()));
		//line = getActionStatus(input);

		logger.destroy();

		line = collectMeasure(macID, stateID);

		if(line.equals("exception")) 
			line = executeActionRecursive(macID, stateID);

		// System.out.println("GATHERED " + line);

		
		return line;
	}

	public String collectMeasures() throws Exception {
		System.out.println("[collectMeasures]: invoked");
		String line = "";

		loggerbuilder = new ProcessBuilder("perl",XPLLOGGER_path);
		loggerbuilder.redirectErrorStream(true);

		logger = loggerbuilder.start();

		ProcessBuilder builder = new ProcessBuilder("perl", "test.pl", "all", "measure");
		builder.redirectErrorStream(true);

		//System.out.println("Calling the script.");

		Process perlproc = builder.start();

		perlproc.waitFor();
		perlproc.destroy(); 


		//  System.out.println("End of the script.");
		BufferedReader i = new BufferedReader(new InputStreamReader(logger.getInputStream()));

		line = getMeasures(i);  

		//logger.waitFor();
		

		logger.destroy();

		return line;
	}

	public String collectMeasure(String macID, String stateID) throws Exception {
		System.out.println("[collectMeasure]: received " + macID + ", " + stateID);

		String line = "";

		loggerbuilder = new ProcessBuilder("perl",XPLLOGGER_path);
		loggerbuilder.redirectErrorStream(true);

		logger = loggerbuilder.start();

		ProcessBuilder builder = new ProcessBuilder("perl", "m.pl", macID, "measure");
		builder.redirectErrorStream(true);

		Process perlproc = builder.start();
		

		perlproc.waitFor();;
		perlproc.destroy();       

		
		
		BufferedReader i = new BufferedReader(new InputStreamReader(logger.getInputStream()));
		line = getMeasure(i, macID, stateID);  

		//logger.waitFor();

		logger.destroy();

		return line;
	}


	private String getMeasures(BufferedReader reader) {
		System.out.println("[getMeasures]: reading the buffer!");

		String result = "";
		StringBuffer sb = new StringBuffer(1000);		 
		try{
			if(reader.ready()){
				char[] buf = new char[1024];
				int numRead = 0;
				numRead = reader.read(buf); 
				if(numRead < 100)
					result = "exception";
				else{
					sb.append(buf, 0, numRead);
					result = "<measures>" + sb.toString()/*.substring(0, sb.toString().length()-4)*/+"</measures>";
					//System.out.println("[getMeasures]: the number of characters is " + numRead);
				}
				reader.close();
			}else
			{
				System.out.println("[getMeasures]: the collection was NOT successful! The buffer is not ready!");
				return "exception";
			}
		}catch(Exception io){
			result= "exception";
			io.printStackTrace();
		}
		return result;
	}

	private String getMeasure(BufferedReader reader, String macID, String stateID) throws IOException {
		System.out.println("[getMeasure]: reading the buffer for " + macID + "," + stateID);
		String result = "";
		StringBuffer sb = new StringBuffer(1000);
		try{
			if(reader.ready()){

				char[] buf = new char[1024];
				int numRead = 0;

				numRead = reader.read(buf); 
				sb.append(buf, 0, numRead);

				reader.close();

				result = sb.toString();

				System.out.println("[getMeasure]: " + result);

				/*
				try{
					JAXBContext ctx = JAXBContext.newInstance(Measures.class);
					Unmarshaller um = ctx.createUnmarshaller();

					Measure m = new Measure();

					m = (Measure) um.unmarshal(new StringReader(result));
					if(m.getMacID().equals(macID)){
						result = Integer.toString(m.getStateID());
					}
					else{
						System.out.println("[getMeasure]: oops! Incorrect measure catched! Trying again.");
						return "exception";
					}

				}catch(JAXBException ja){
					ja.printStackTrace();
				}*/
			}else{
				System.out.println("[getMeasure]: oops! The reader is not ready! Trying again.");
				result = "exception";
			}
		}catch(IOException e){
			e.printStackTrace();
		}

		return result;
	}

	private String getActionStatus(BufferedReader reader) throws IOException {
		String result = "";
		StringBuffer sb = new StringBuffer(1000);		 
		char[] buf = new char[1024];
		int numRead = 0;

		numRead = reader.read(buf); 
		result = sb.append(buf, 0, numRead).toString();

		reader.close();
		return result;
	}
}
