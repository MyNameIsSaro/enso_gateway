package plugwise.core;
import com.google.gson.*;


public class PlugStatus {
	
	public PlugStatus()
	{
		this.mac = "000000";
		this.currentPower = 0;
		this.current8Power = 0;
		this.units = "none";

	}
	
	public PlugStatus(String mac, double currentPower, double current8Power,
			String units) {
		super();
		this.mac = mac;
		this.currentPower = currentPower;
		this.current8Power = current8Power;
		this.units = units;
	}
	public String mac;
	public double currentPower;
	public double current8Power;
	public String units;
	
	public String toString()
	{
		return new Gson().toJson(this);
	}
}
