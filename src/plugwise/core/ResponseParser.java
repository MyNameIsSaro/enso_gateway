package plugwise.core;

import java.io.*;
import java.util.LinkedList;

public class ResponseParser {

	public static PlugStatus parsePlugLivepower(BufferedReader reader)
	{
		String line;
		String output = "";
		PlugStatus status;
		String mac = "000000";
		double value = 0;
		double value8 = 0;
		String units = "kW";
		try
		{
			while ((line = reader.readLine()) != null)
			{
				output += (line + '\n');
				if (line.matches(".*'device',$"))
				{
					mac = reader.readLine().replaceAll("\\W", "");
					//System.out.println("MAC:" + mac);
				}
				if (line.matches(".*'current',$"))
				{
					
					value = Double.parseDouble(reader.readLine().replaceAll("[^\\w\\.]", ""));
					//System.out.println("current:" + value);
				}
				if (line.matches(".*'current8',$"))
				{
					value8 = Double.parseDouble(reader.readLine().replaceAll("[^\\w\\.]", ""));
					//System.out.println("current8:" + value8);
				}
				if (line.matches(".*'units',$"))
				{
					units = reader.readLine().replaceAll("\\W", "");
					//System.out.println("units:" + units);
				}
			}
		if (mac.equalsIgnoreCase("000000"))
			return null;
		return new PlugStatus(mac, value, value8, units); 
		} catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static LinkedList<PlugStatus> parseAllPlugsLivepower (BufferedReader reader, String MACArray[])
	{
		String line;
		LinkedList<PlugStatus> list = new LinkedList<PlugStatus>();
		PlugStatus current = null;
		try
		{
			while ((line = reader.readLine()) != null)
			{
				if (line.matches(".*'device',$"))
				{
					current = new PlugStatus();
					current.mac = reader.readLine().replaceAll("\\W", "");
					//System.out.println("MAC:" + mac);
				}
				if (line.matches(".*'current',$") && current != null)
				{
					
					current.currentPower = Double.parseDouble(reader.readLine().replaceAll("[^\\w\\.]", ""));
					//System.out.println("current:" + value);
				}
				if (line.matches(".*'current8',$") && current != null)
				{
					current.current8Power = Double.parseDouble(reader.readLine().replaceAll("[^\\w\\.]", ""));
					//System.out.println("current8:" + value8);
				}
				if (line.matches(".*'units',$") && current != null)
				{
					current.units = reader.readLine().replaceAll("\\W", "");
					list.add(current);
					current = null;
					//System.out.println("units:" + units);
				}
			}
		return list;
		} catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try{
			  // Open the file that is the first 
			  // command line parameter
			  FileInputStream fstream = new FileInputStream("/home/saro/loooog");
			  // Get the object of DataInputStream
			  DataInputStream in = new DataInputStream(fstream);
			  BufferedReader br = new BufferedReader(new InputStreamReader(in));
			  System.out.println(ResponseParser.parsePlugLivepower(br));
		} catch (Exception e)
		{
			e.printStackTrace();
		}

	}

}
