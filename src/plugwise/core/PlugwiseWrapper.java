package plugwise.core;
import java.io.*;
import java.util.LinkedList;


/**
 * This class is a wrapper to read and force(TODO) plugwise devices status.
 *   
 * 
 * @author Rosario Contarino <contarino.rosario@gmail.com>
 *
 */
public class PlugwiseWrapper {
	
	private String SCRIPT_PATH = "/usr/bin/site_perl/plugwise.pl";
	private String serialPort = "/dev/ttyUSB0";
	
	public PlugwiseWrapper(){}
	
	public PlugwiseWrapper(String serialPort)
	{
		this.serialPort = serialPort;
	}
	
	/**
	 * Turn ON/OFF the plug using plugwise.pl
	 * @param MAC MAC Address of the target plug.
	 * @param state true to force the plug ON, false to disable the plug
	 * @return true if action will succeed, false otherwise.
	 */
	public boolean forcePlugState(String MAC, boolean state)
	{
		return false;
	}

	/**
	 * Toogle plug status.
	 * @param MAC MAC Address of the target plug.
	 * @return the current status of plug.
	 */
	public boolean tooglePlugState(String MAC)
	{
		return false;
	}
	
	/**
	 * Read livepower status of the target plug.
	 * @param MAC
	 * @return a PlugStatus object containing the energy information provided by the plug.
	 */
	public PlugStatus livepowerPlug(String MAC)
	{
		System.out.println("[Livepower on " + MAC +" ]");
		try {
			ProcessBuilder loggerbuilder = new ProcessBuilder("perl", SCRIPT_PATH, serialPort, "livepower", MAC);
			//loggerbuilder.redirectErrorStream(true);
			Process logger = loggerbuilder.start();

			BufferedReader reader = new BufferedReader(new InputStreamReader(logger.getInputStream()));
			
			return ResponseParser.parsePlugLivepower(reader);
		} catch (IOException e){
			e.printStackTrace();
			return null;
		}
	}
	
	
	public LinkedList<PlugStatus> livepowerAllPlugs(String MACs)
	{
		System.out.println("[Livepower on " + MACs +" ]");
		String MACArray[] = MACs.split(",");
		try {
			ProcessBuilder loggerbuilder = new ProcessBuilder("perl", SCRIPT_PATH, serialPort, "livepower", MACs);
			//loggerbuilder.redirectErrorStream(true);
			Process logger = loggerbuilder.start();

			BufferedReader reader = new BufferedReader(new InputStreamReader(logger.getInputStream()));
			
			return ResponseParser.parseAllPlugsLivepower(reader, MACArray);
		} catch (IOException e){
			e.printStackTrace();
			return null;
		}
	}
	
	

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		PlugwiseWrapper pw = new PlugwiseWrapper("/dev/ttyUSB1");
		LinkedList<PlugStatus> list = pw.livepowerAllPlugs("98C0A0,B7BED0,B83221,B83228");
		System.out.println(list);

	}

}
